#pragma once

#include <bit>
#include <utility>
#include <vector>
#include <compare>

namespace mtmath {
  /**
   * Represents an array of bytes
   * Allows bit operations to be done on the whole byte array
   * Useful for operating on arbitrarily large amounts of bits
   *
   * Bytes are stored in "little endian" order internally
   */
  class ByteArray {
    std::vector<uint8_t> bytes;
  public:
    /**
     * Create an empty byte array
     */
    ByteArray() = default;

    /**
     * Create an array from specific bytes
     * @param bytes
     */
    explicit ByteArray(std::vector<uint8_t> bytes) : bytes(std::move(bytes)) {}

    /**
     * Constant binary "and" with a vector of bytes
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator&(const std::vector<uint8_t>& b) const noexcept;

    /**
     * Constant binary "or" with a vector of bytes
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator|(const std::vector<uint8_t>& b) const noexcept;

    /**
     * Constant binary "xor" with a vector of bytes
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator^(const std::vector<uint8_t>& b) const noexcept;

    /**
     * In place binary "and" with a vector of bytes
     * @param b
     * @return
     */
    ByteArray& operator&=(const std::vector<uint8_t>& b) noexcept;

    /**
     * In place binary "or" with a vector of bytes
     * @param b
     * @return
     */
    ByteArray& operator|=(const std::vector<uint8_t>& b) noexcept;

    /**
     * In place binary "xor" with a vector of bytes
     * @param b
     * @return
     */
    ByteArray& operator^=(const std::vector<uint8_t>& b) noexcept;

    /**
     * Constant binary "and" with another byte array
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator&(const ByteArray& b) const noexcept { return *this & b.bytes; }

    /**
     * In place binary "and" with another byte array
     * @param b
     * @return
     */
    ByteArray& operator&=(const ByteArray& b) noexcept { return *this &= b.bytes; }

    /**
     * Constant binary "or" with another byte array
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator|(const ByteArray& b) const noexcept { return *this | b.bytes; }

    /**
     * In place binary "or" with another byte array
     * @param b
     * @return
     */
    ByteArray& operator|=(const ByteArray& b) noexcept { return *this |= b.bytes; }

    /**
     * Constant binary "xor" with another byte array
     * @param b
     * @return
     */
    [[nodiscard]] ByteArray operator^(const ByteArray& b) const noexcept { return *this ^ b.bytes; }

    /**
     * In place binary "xor" with another byte array
     * @param b
     * @return
     */
    ByteArray& operator^=(const ByteArray& b) noexcept { return *this ^= b.bytes; }

    /**
     * Constant left shift operator
     * @param amount Number of bits to shift by
     * @return
     */
    [[nodiscard]] ByteArray operator<<(size_t amount) const;

    /**
     * In place left shift operator
     * @param amount Number of bits to shift by
     * @return
     */
    ByteArray& operator<<=(size_t amount);

    /**
     * Constant right shift operator
     * @param amount Number of bits to shift by
     * @return
     */
    [[nodiscard]] ByteArray operator>>(size_t amount) const;

    /**
     * In place left shift operator
     * @param amount Number of bits to shift by
     * @return
     */
    ByteArray& operator>>=(size_t amount);

    /**
     * Three way comparison operator
     * @param o
     * @return
     */
    [[nodiscard]] std::strong_ordering operator<=>(const ByteArray& o) const noexcept;

    /**
     * Equality operator
     * @param o
     * @return
     */
    bool operator==(const ByteArray& o) const noexcept { return *this <=> o == std::strong_ordering::equal; }

    /**
     * Access a byte by reference at the specified index
     * @param i
     * @return
     */
    uint8_t& operator[](size_t i) { return at(i); }

    /**
     * Access a byte by constant reference at the specified index. Constant
     * @param i
     * @return
     */
    [[nodiscard]] const uint8_t& operator[](size_t i) const noexcept { return at(i); }

    /**
     * Access a byte by reference at the specified index
     * @param i
     * @return
     */
    uint8_t& at(size_t i) { return bytes.at(i); }

    /**
     * Access a byte at by constant reference the specified index. Constant
     * @param i
     * @return
     */
    [[nodiscard]] const uint8_t& at(size_t i) const { return bytes.at(i); }

    /**
     * Get the value of the byte at the specified index, or if the index is out of bounds get 0
     * @param i Index to retrieve a byte from
     * @return
     */
    [[nodiscard]] uint8_t get(size_t i) const noexcept { return i < bytes.size() ? bytes.at(i) : 0U; }

    /**
     * Add a byte to the back of the byte array
     * @param byte
     * @return
     */
    ByteArray& emplace_back(uint8_t byte) { bytes.emplace_back(byte); return *this; }

    /**
     * Reserve a number of bytes in the byte array
     * @param size
     * @return
     */
    ByteArray& reserve(size_t size) { bytes.reserve(size); return *this; }

    /**
     * Resize the byte array to a specific size
     * @param size
     * @return
     */
    ByteArray& resize(size_t size) { bytes.resize(size); return *this; }

    /**
     * Shrink the capacity of the byte array to match the number of used bytes
     * @return
     */
    ByteArray& shrink_to_fit() {bytes.shrink_to_fit(); return *this;}

    /**
     * Return the capacity of the byte array
     * @return
     */
    [[nodiscard]] size_t capacity() const noexcept { return bytes.capacity(); }

    /**
     * Return the size of the byte array
     * @return
     */
    [[nodiscard]] size_t size() const noexcept { return bytes.size(); }

    /**
     * Return a const iterator to the beginning of the byte array
     * @return
     */
    [[nodiscard]] decltype(auto) begin() const { return bytes.begin(); }

    /**
     * Return an iterator to the beginning of the byte array
     * @return
     */
    decltype(auto) begin() { return bytes.begin(); }

    /**
     * Return a const iterator to the end of the byte array
     * @return
     */
    [[nodiscard]] decltype(auto) end() const { return bytes.end(); }

    /**
     * Return an iterator to the end of the byte array
     * @return
     */
    decltype(auto) end() { return bytes.end(); }

    /**
     * Fill the specified bytes with zeros
     * @param begin
     * @param end
     */
    void zero_fill(decltype(bytes)::iterator begin, decltype(bytes)::iterator end) {
      std::fill(std::move(begin), std::move(end), 0);
    }

    /**
     * Fill the specified bytes with zeros and simplify the byte array
     * @param begin
     * @param end
     */
    void zero_fill_and_simplify(decltype(bytes)::iterator begin, decltype(bytes)::iterator end) {
      zero_fill(std::move(begin), std::move(end));
      simplify();
    }

    /**
     * Erase the specified bytes from the byte array
     * @param begin
     * @param end
     */
    void erase(decltype(bytes)::iterator begin, decltype(bytes)::iterator end) {
      bytes.erase(std::move(begin), std::move(end));
    }

    /**
     * Simplify the byte array by removing trailing zero bytes (for a number, this is leading zero bytes)
     */
    void simplify();

    /**
     * Clear the byte array
     */
    void clear() {
      bytes.clear();
    }

    /**
     * Return whether the byte array is empty
     * @return
     */
    [[nodiscard]] bool empty() const { return bytes.empty(); }

    /**
     * Casts the byte array to a specific type
     * The type must be default constructible
     * Will simply do a memcpy onto the bytes of the type
     * If there are more bytes in the byte array
     * @tparam T
     * @return
     */
    template <typename T>
    T as() const noexcept {
      T res{};
      auto copyBytes = std::min(sizeof(res), bytes.size());
      auto copyDelta = sizeof(res) - copyBytes;
      if (copyDelta) {
        // Since we have "infinite" trailing zero bytes, we zero out our memory
        memset(static_cast<void *>(&res), 0, sizeof(res));
      }

      if constexpr (std::endian::native == std::endian::little) {
        // little endian logic, just memcopy
        memcpy(static_cast<void*>(&res), static_cast<const void*>(bytes.data()), copyBytes);
      }
      else {
        // For big endian, we first have to reverse the bytes
        auto rcopy = bytes;
        std::reverse(rcopy.begin(), rcopy.end());

        // Get our offset pointer. Again, since we're reversed, our "trailing" zeros become our "leading" zeroes
        void* ptr = static_cast<void*>(static_cast<uint8_t*>(static_cast<void*>(&res)) + copyDelta);
        memcpy(ptr, static_cast<const void*>(rcopy.data()), copyBytes);
      }

      return res;
    }

    /**
     * Create a byte array from a specific value
     * Does a memcopy from the value and then simplifies
     * @tparam T
     * @param value
     * @return
     */
    template <typename T>
    static ByteArray from(T value) {
      ByteArray res;
      res.bytes.resize(sizeof(value));

      memcpy(&res.bytes[0], &value, std::min(sizeof(res), res.bytes.size()));

      // For big endian, we need to reverse our bytes
      if constexpr (std::endian::native != std::endian::little) {
        std::reverse(res.bytes.begin(), res.bytes.end());
      }

      // Make sure we simplify to reduce our memory footprint
      res.simplify();
      return res;
    }
  };
}
