#pragma once

#include <vector>
#include <string>
#include <compare>
#include <tuple>
#include <optional>
#include <stdexcept>
#include "data_structures/byte_array.h"
#include "mtmath_c.h"
#include <memory>
#include <ostream>

namespace mtmath {
  class BigInt;

  namespace c {
    /**
     * Converts a C++ big int into a C big int
     * @param bi
     * @param out
     */
    void into(const mtmath::BigInt& bi, MtMath_BigInt* out);

    /**
     * Converts a C big int into a C++ big int
     * @param cbi
     * @param out
     */
    void into(const MtMath_BigInt& cbi, mtmath::BigInt* out);
  }

  namespace immut {
    /**
     * Immutable big integer
     * Does structural sharing when possible
     */
    class BigInt;
  }

  /**
   * Represents a big integer
   * Invalid big integers are equivalent to "not finite". It happens with divisions by 0
   */
  class BigInt {
    /**
     * Big integer flags
     */
    enum FLAGS {
      NEGATIVE = 0x1,
      INVALID = 0x2
    };

    uint8_t flags = 0x0;
    ByteArray digits = {};

    /**
     * Private, low level constructor using flags and byte arrays
     * @param flags
     * @param digits
     */
    BigInt(uint8_t flags, ByteArray digits) : flags(flags), digits(std::move(digits)) {}

  public:
    /**
     * Creates a new big integer (set to zero)
     */
    BigInt() = default;

    /**
     * Copy constructor
     * @param other
     */
    BigInt(const BigInt& other) = default;

    /**
     * Move constructor
     * @param other
     */
    BigInt(BigInt&& other) noexcept : flags(other.flags), digits(std::move(other.digits)) {}

    /**
     * Copy assignment
     * @param other
     * @return
     */
    BigInt& operator=(const BigInt& other) = default;

    /**
     * Move assignment
     * @param other
     * @return
     */
    BigInt& operator=(BigInt&& other) noexcept { flags = other.flags; std::swap(digits, other.digits); return *this; }

    /**
     * Create a big integer from a string using a specific base
     * @tparam T std::string, std::string_view, char*, or const char*
     * @param numberString Numeric string representing the integer
     * @param base Radix base of the string
     */
    template<typename T>
    BigInt(const T&numberString, int base) {
      static_assert(std::is_same_v<std::decay_t<T>, std::string> || std::is_same_v<std::decay_t<T>, std::string_view>
                      || std::is_same_v<std::decay_t<T>, char*>|| std::is_same_v<std::decay_t<T>, const char*>,
                    "Can only do base conversion with string types!");

      if (base < 2 || base > 36) {
        throw std::runtime_error("BigInt must come from strings in a base between 2 and 36.");
      }

      auto process_char = [&](auto ch) {
        if (!std::isdigit(ch)) {
          if (base <= 10 || !std::isalpha(ch)) {
            return false;
          }
          else {
            // to lowercase
            ch |= 1 << 5;
            if (ch - 'a' <= base - 10) {
              digits.emplace_back(ch - 'a' + 10);
            }
          }
        }
        else if (ch - '0' < base) {
          digits.emplace_back(ch - '0');
        }
        else {
          return false;
        }
        return true;
      };

      if constexpr (std::is_same_v<std::decay_t<T>, std::string> || std::is_same_v<std::decay_t<T>, std::string_view>) {
        digits.reserve(numberString.size());
        if (!numberString.empty()) {
          if (numberString[0] == '-') {
            flags |= NEGATIVE;
          }
        }
        for(size_t i = (numberString[0] == '-' || numberString[0] == '+'); i < numberString.size(); ++i) {
          if (!process_char(numberString[i])) {
            break;
          }
        }
        compress(base);
      }
      else if constexpr (std::is_same_v<std::decay_t<T>, char*>|| std::is_same_v<std::decay_t<T>, const char*>) {
        if (numberString[0] == '-') {
          flags |= NEGATIVE;
        }
        for(size_t i = (numberString[0] == '-' || numberString[0] == '+'); ; ++i) {
          if (!process_char(numberString[i])) {
            break;
          }
        }
        compress(base);
      }
      simplify();
    }

    /**
     * Create a new big integer from a base-10 radix string
     * @param s numeric string
     */
    explicit BigInt(const std::string& s) : BigInt(s, 10) {}

    /**
     * Create a new big integer from a base-10 radix string
     * @param s numeric string
     */
    explicit BigInt(const std::string_view& s) : BigInt(s, 10) {}

    /**
     * Create a new big integer from a base-10 radix string
     * @param s numeric string
     */
    explicit BigInt(const char* c) : BigInt(c, 10) {}

    /**
     * Create a new big integer from a number
     * @tparam T Integer type
     * @param number Number to use
     */
    template<typename T>
    BigInt(const T& number) {
      static_assert(std::numeric_limits<T>::is_integer, "Can only initialize from strings and integers");
      auto n = number;
      if (n < 0) {
        flags |= NEGATIVE;
        n *= -1;
      }
      while (n > 0) {
        digits.emplace_back(n & std::numeric_limits<uint8_t>::max());
        n >>= std::numeric_limits<uint8_t>::digits;
      }
      simplify();
    }

    /**
     * Create a 0 big int
     * @return
     */
    static BigInt zero() { return BigInt{}; }

    /**
     * Create a 1 big int
     * @return
     */
    static BigInt one() { return BigInt{1}; }

    /**
     * Create a 2 big int
     * @return
     */
    static BigInt two() { return BigInt{2}; }

    /**
     * Create an invalid big int
     * @return
     */
    static BigInt invalid() { return BigInt{INVALID, ByteArray{}}; }

    /**
     * Returns if the big integer represents 0
     * @return
     */
    [[nodiscard]] bool is_zero() const noexcept { return digits.empty(); }

    /**
     * Returns if the big integer is valid
     * @return
     */
    [[nodiscard]] bool is_valid() const noexcept { return !(flags & INVALID); }

    /**
     * Returns if the big integer is negative
     * @return
     */
    [[nodiscard]] bool is_negative() const noexcept { return flags & NEGATIVE; }

    /**
     * In place absolute value operator
     * @return
     */
    BigInt& abs() noexcept { flags &= ~NEGATIVE; return *this; }

    /**
     * Const absolute value operator
     * @return
     */
    [[nodiscard]] BigInt abs_val() const noexcept { auto copy = *this; return copy.abs(); }

    /**
     * Converts the big integer into a string for a given base
     * If the base is not supported, will be std::nullopt
     * @param base A number between 2 and 32 (inclusive)
     * @return
     */
    [[nodiscard]] std::optional<std::string> to_string(int base) const;

    /**
     * Converts the big integer into a 64-bit int. Will truncate if too big.
     * @return
     */
    [[nodiscard]] int64_t as_i64() const noexcept;

    /**
     * Const negation operator
     * @return
     */
    BigInt operator-() const;

    /**
     * In place addition operator
     * @param o
     * @return
     */
    BigInt& operator+=(const BigInt& o) noexcept;

    /**
     * In place subtraction operator
     * @param o
     * @return
     */
    BigInt& operator-=(const BigInt& o) noexcept;

    /**
     * In place division operator
     * @param o
     * @return
     */
    BigInt& operator/=(const BigInt& o) noexcept;

    /**
     * In place remainder operator
     * @param o
     * @return
     */
    BigInt& operator%=(const BigInt& o) noexcept;

    /**
     * In place multiplication operator
     * @param o
     * @return
     */
    BigInt& operator*=(const BigInt& o) noexcept;

    /**
     * Constant addition operator
     * @param o
     * @return
     */
    [[nodiscard]] BigInt operator+(const BigInt& o) const { auto copy = *this; return copy += o; }

    /**
     * Constant subtraction operator
     * @param o
     * @return
     */
    [[nodiscard]] BigInt operator-(const BigInt& o) const { auto copy = *this; return copy -= o; }

    /**
     * Constant division operator
     * @param o
     * @return
     */
    [[nodiscard]] BigInt operator/(const BigInt& o) const { auto copy = *this; return copy /= o; }

    /**
     * Constant remainder operator
     * @param o
     * @return
     */
    [[nodiscard]] BigInt operator%(const BigInt& o) const { auto copy = *this; return copy %= o; }

    /**
     * Constant multiplication operator
     * @param o
     * @return
     */
    [[nodiscard]] BigInt operator*(const BigInt& o) const { auto copy = *this; return copy *= o; }

    /**
     * Does a division and returns a tuple in the form of [remainder, quotient]
     * @param denominator
     * @return
     */
    [[nodiscard]] std::tuple<BigInt, BigInt> divide(const BigInt& denominator) const noexcept;

    /**
     * Three way comparison operator
     * Invalid values are considered "equal"
     * @param o
     * @return
     */
    [[nodiscard]] std::strong_ordering operator<=>(const BigInt& o) const noexcept;

    /**
     * Equavlence operator
     * @param o
     * @return
     */
    [[nodiscard]] bool operator==(const BigInt& o) const noexcept { return *this <=> o == std::strong_ordering::equal; }

    /**
     * Constant right shift operator
     * @param i Amount to shift right
     * @return
     */
    [[nodiscard]] BigInt operator>>(size_t i) const { auto copy = *this; copy >>= i; return copy; }

    /**
     * Constant left shift operator
     * @param i Amount to shift left
     * @return
     */
    [[nodiscard]] BigInt operator<<(size_t i) const { auto copy = *this; copy <<= i; return copy; }

    /**
     * In place left shift operator
     * @param i Amount to shift left
     * @return
     */
    BigInt& operator<<=(size_t i);

    /**
     * In place right shift operator
     * @param i Amount to shift right
     * @return
     */
    BigInt& operator>>=(size_t i);

    /**
     * Converts to an immutable big integer
     * @return
     */
    [[nodiscard]] immut::BigInt to_immut() const;

    friend ::mtmath::immut::BigInt;
    friend void ::mtmath::c::into(const BigInt& bi, MtMath_BigInt* out);
    friend void ::mtmath::c::into(const MtMath_BigInt& cbi, BigInt* out);

  private:
    void simplify();
    void compress(int base);
    int abs_compare(const BigInt& o) const noexcept;
  };

  namespace immut {
    /**
     * Immutable big integer
     * Invalid values are a result of "divide by zero" and are considered equal to other invalid values
     */
    class BigInt {
      enum FLAGS {
        NEGATIVE = 0x1,
        INVALID = 0x2
      };

      uint8_t flags = 0x0;
      std::shared_ptr<ByteArray> digits = std::make_shared<ByteArray>();
      BigInt(uint8_t flags, std::shared_ptr<ByteArray> digits);

      static BigInt zeroConst;
      static BigInt oneConst;
      static BigInt twoConst;
      static BigInt invalidConst;

      static BigInt fresh();

    public:
      /**
       * Create a new immutable big int equal to 0
       */
      BigInt();

      /**
       * Copy constructor
       * @param other
       */
      BigInt(const BigInt& other);

      /**
       * Move constructor
       * @param other
       */
      BigInt(BigInt&& other) noexcept;

      /**
       * Copy assignment
       * @param other
       * @return
       */
      BigInt& operator=(const BigInt& other);

      /**
       * Creates an instance from a numeric string in a specified radix base
       * @tparam T std::string, std::string_view, const char*, or char*
       * @param numberString Numeric string representation
       * @param base Radix base
       */
      template<typename T>
      BigInt(const T&numberString, int base) {
        static_assert(std::is_same_v<std::decay_t<T>, std::string> || std::is_same_v<std::decay_t<T>, std::string_view>
                        || std::is_same_v<std::decay_t<T>, char*>|| std::is_same_v<std::decay_t<T>, const char*>,
                      "Can only do base conversion with string types!");

        if (base < 2 || base > 36) {
          throw std::runtime_error("BigInt must come from strings in a base between 2 and 36.");
        }

        auto process_char = [&](auto ch) {
          if (!std::isdigit(ch)) {
            if (base <= 10 || !std::isalpha(ch)) {
              return false;
            }
            else {
              // to lowercase
              ch |= 1 << 5;
              if (ch - 'a' <= base - 10) {
                digits->emplace_back(ch - 'a' + 10);
              }
            }
          }
          else if (ch - '0' < base) {
            digits->emplace_back(ch - '0');
          }
          else {
            return false;
          }
          return true;
        };

        if constexpr (std::is_same_v<std::decay_t<T>, std::string> || std::is_same_v<std::decay_t<T>, std::string_view>) {
          digits->reserve(numberString.size());
          if (!numberString.empty()) {
            if (numberString[0] == '-') {
              flags |= NEGATIVE;
            }
          }
          for(size_t i = (numberString[0] == '-' || numberString[0] == '+'); i < numberString.size(); ++i) {
            if (!process_char(numberString[i])) {
              break;
            }
          }
          compress(base);
        }
        else if constexpr (std::is_same_v<std::decay_t<T>, char*>|| std::is_same_v<std::decay_t<T>, const char*>) {
          if (numberString[0] == '-') {
            flags |= NEGATIVE;
          }
          for(size_t i = (numberString[0] == '-' || numberString[0] == '+'); ; ++i) {
            if (!process_char(numberString[i])) {
              break;
            }
          }
          compress(base);
        }
        simplify();
      }

      /**
       * Creates an instance from a base-10 numeric string
       * @param s
       */
      explicit BigInt(const std::string& s) : BigInt(s, 10) {}

      /**
       * Creates an instance from a base-10 numeric string
       * @param s
       */
      explicit BigInt(const std::string_view& s) : BigInt(s, 10) {}

      /**
       * Creates an instance from a base-10 numeric string
       * @param s
       */
      explicit BigInt(const char* c) : BigInt(c, 10) {}

      /**
       * Creates an instance from an integer number (e.g. long, int, short, etc)
       * @tparam T
       * @param number
       */
      template<typename T>
      BigInt(const T& number) {
        static_assert(std::numeric_limits<T>::is_integer, "Can only initialize from strings and integers");
        auto n = number;
        if (n < 0) {
          flags |= NEGATIVE;
          n *= -1;
        }
        while (n > 0) {
          digits->emplace_back(n & std::numeric_limits<uint8_t>::max());
          n >>= std::numeric_limits<uint8_t>::digits;
        }
        simplify();
      }

      /**
       * Move assignment
       * @param other
       * @return
       */
      BigInt& operator=(BigInt&& other) noexcept;

      /**
       * Get a 0 immutable big int
       * @return
       */
      static BigInt zero();

      /**
       * Get a 1 immutable big int
       * @return
       */
      static BigInt one();

      /**
       * Get a 2 immutable big int
       * @return
       */
      static BigInt two();

      /**
       * Get an invalid immutable big int
       * @return
       */
      static BigInt invalid();

      /**
       * Returns whether it is equal to zero
       * @return
       */
      [[nodiscard]] bool is_zero() const noexcept;

      /**
       * Returns whether it is valid
       * @return
       */
      [[nodiscard]] bool is_valid() const noexcept;

      /**
       * Returns whether it is negative
       * @return
       */
      [[nodiscard]] bool is_negative() const noexcept;

      /**
       * Absolute value of the immutable big int
       * @return
       */
      [[nodiscard]] BigInt abs() const noexcept;

      /**
       * Alias of abs
       * @return
       */
      [[nodiscard]] BigInt abs_val() const noexcept { return abs(); }

      /**
       * Casts to a 64-bit int. If too big, will truncate
       * @return
       */
      [[nodiscard]] int64_t as_i64() const noexcept;

      /**
       * Casts to a string with a specific base radix (between 2 and 32 inclusive)
       * If radix is invalid, will return a null optional
       * @param base
       * @return
       */
      [[nodiscard]] std::optional<std::string> to_string(int base) const;

      /**
       * Negation operator
       * @return
       */
      BigInt operator-() const;

      /**
       * Addition operator
       * @param o
       * @return
       */
      BigInt operator+(const BigInt& o) const noexcept;

      /**
       * Subtraction operator
       * @param o
       * @return
       */
      BigInt operator-(const BigInt& o) const noexcept;

      /**
       * Division operator
       * @param o
       * @return
       */
      BigInt operator/(const BigInt& o) const noexcept;

      /**
       * Remainder operator
       * @param o
       * @return
       */
      BigInt operator%(const BigInt& o) const noexcept;

      /**
       * Multiplication operator
       * @param o
       * @return
       */
      BigInt operator*(const BigInt& o) const noexcept;

      /**
       * Does a division and then returns a tuple of [remainder, quotient]
       * @param denominator
       * @return
       */
      [[nodiscard]] std::tuple<BigInt, BigInt> divide(const BigInt& denominator) const noexcept;

      /**
       * Three way comparison operator
       * Invalid big ints are considered equal
       * @param o
       * @return
       */
      [[nodiscard]] std::strong_ordering operator<=>(const BigInt& o) const noexcept;

      /**
       * Equality operator
       * @param o
       * @return
       */
      [[nodiscard]] bool operator==(const BigInt& o) const noexcept { return *this <=> o == std::strong_ordering::equal; }

      /**
       * Left shift operator
       * @param i Amount to shift left by
       * @return
       */
      [[nodiscard]] BigInt operator<<(size_t i) const noexcept;

      /**
       * Right shift operator
       * @param i Amount to shift right by
       * @return
       */
      [[nodiscard]] BigInt operator>>(size_t i) const noexcept;

      friend ::mtmath::BigInt;

      /**
       * Casts to a mutable big integer
       * @return
       */
      [[nodiscard]] ::mtmath::BigInt to_mut() const;

    private:
      void simplify();
      void compress(int base);
      [[nodiscard]] bool abs_less_than(const BigInt& o) const noexcept;
    };
  }
}

template<>
class std::numeric_limits<mtmath::BigInt> {
public:
  using Type = mtmath::BigInt;
  static constexpr bool is_specialized = true;
  static constexpr bool is_signed = true;
  static constexpr bool is_integer = true;
  static constexpr bool is_exact = true;
  static constexpr bool has_infinity = false;
  static constexpr bool has_quiet_NaN = true;
  static constexpr bool has_signaling_NaN = false;
  static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
  static constexpr bool has_denorm_loss = false;
  static constexpr std::float_round_style round_style = std::round_toward_zero;
  static constexpr bool is_iec559 = false;
  static constexpr bool is_bounded = false;
  static constexpr bool is_modulo = false;
  static constexpr int digits = std::numeric_limits<int>::max();
  static constexpr int digits10 = std::numeric_limits<int>::max();
  static constexpr int max_digits10 = std::numeric_limits<int>::max();
  static constexpr int radix = 2;
  static constexpr int min_exponent = 0;
  static constexpr int min_exponent10 = 0;
  static constexpr int max_exponent = 0;
  static constexpr int max_exponent10 = 0;
  static constexpr bool traps = false;
  static constexpr bool tinyness_before = false;

  static Type min() {
    return Type{ 0 };
  }

  static Type lowest() {
    return Type{ 0 };
  }

  static Type epsilon() {
    return Type {1};
  }

  static Type round_error() {
    return Type {0};
  }

  static Type infinity() {
    return Type { 0 };
  }

  static Type quiet_NaN() {
    return Type::invalid();
  }

  static Type signaling_NaN() {
    return quiet_NaN();
  }

  static Type denorm_min() {
    return min();
  }
};

template<>
class std::numeric_limits<mtmath::immut::BigInt> {
public:
  using Type = mtmath::immut::BigInt;
  static constexpr bool is_specialized = true;
  static constexpr bool is_signed = true;
  static constexpr bool is_integer = true;
  static constexpr bool is_exact = true;
  static constexpr bool has_infinity = false;
  static constexpr bool has_quiet_NaN = true;
  static constexpr bool has_signaling_NaN = false;
  static constexpr std::float_denorm_style has_denorm = std::denorm_absent;
  static constexpr bool has_denorm_loss = false;
  static constexpr std::float_round_style round_style = std::round_toward_zero;
  static constexpr bool is_iec559 = false;
  static constexpr bool is_bounded = false;
  static constexpr bool is_modulo = false;
  static constexpr int digits = std::numeric_limits<int>::max();
  static constexpr int digits10 = std::numeric_limits<int>::max();
  static constexpr int max_digits10 = std::numeric_limits<int>::max();
  static constexpr int radix = 2;
  static constexpr int min_exponent = 0;
  static constexpr int min_exponent10 = 0;
  static constexpr int max_exponent = 0;
  static constexpr int max_exponent10 = 0;
  static constexpr bool traps = false;
  static constexpr bool tinyness_before = false;

  static Type min() {
    return Type::zero();
  }

  static Type lowest() {
    return Type::zero();
  }

  static Type epsilon() {
    return Type::one();
  }

  static Type round_error() {
    return Type::zero();
  }

  static Type infinity() {
    return Type::zero();
  }

  static Type quiet_NaN() {
    return Type::invalid();
  }

  static Type signaling_NaN() {
    return quiet_NaN();
  }

  static Type denorm_min() {
    return min();
  }
};

inline std::ostream& operator<< (std::ostream& o, const mtmath::BigInt& b)
{
  o << *b.to_string(10);
  return o;
}

inline std::ostream& operator<< (std::ostream& o, const mtmath::immut::BigInt& b)
{
  o << *b.to_string(10);
  return o;
}
