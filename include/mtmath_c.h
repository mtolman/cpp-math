#ifndef MATH_MTMATH_C_H
#define MATH_MTMATH_C_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * C-struct that represents an array of bytes
 */
typedef struct MtMath_ByteArray {
  unsigned long long len;
  unsigned char* bytes;
} MtMath_ByteArray;

/**
 * C-struct that represents a big integer
 */
typedef struct MtMath_BigInt {
  unsigned char flags;
  MtMath_ByteArray digits;
} MtMath_BigInt;

/**
 * C-struct that represents a rational number
 */
struct MtMath_Rational {
  MtMath_BigInt numerator;
  MtMath_BigInt denominator;
};

/**
 * Initializes a byte array
 * @param ba Pointer to a byte array to initialize
 */
extern void init_byte_array(MtMath_ByteArray* ba);

/**
 * Initializes a big integer
 * @param bi Pointer to a bit int to initialize
 */
extern void init_big_int(MtMath_BigInt* bi);

/**
 * Initializes a rational number
 * @param ra Pointer to a rational to initialize
 */
extern void init_rational(MtMath_Rational* ra);

/**
 * Sets a big integer to the value of a string
 * Will not read past strlen bytes
 * @param str C-string to read. If empty or null, will set out to 0
 * @param strlen Length of string
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_str_safe(const char* str, unsigned long long strlen, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of a string. Relies on null terminator.
 * @param str C-string to read. If empty or null, will set out to 0
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_str(const char* str, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of an integer
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_int(int val, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of a long
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_long(long val, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of a long long
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_long_long(long long val, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of an unsigned int
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_uint(unsigned int val, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of an unsigned long
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_ulong(unsigned long val, MtMath_BigInt* out);

/**
 * Sets a big integer to the value of an unsigned long long
 * @param int Integer value to use
 * @param out Pointer to big int to change
 */
extern void set_big_int_to_ulong_long(unsigned long long val, MtMath_BigInt* out);

/**
 * Add two big integers and save the result to out
 * @param left Left operand
 * @param right Right operand
 * @param out Output
 */
extern void add_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* out);

/**
 * Subtract two big integers and save the result to out
 * @param left Left operand
 * @param right Right operand
 * @param out Output
 */
extern void sub_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* out);

/**
 * Multiply two big integers and save the result to out
 * @param left Left operand
 * @param right Right operand
 * @param out Output
 */
extern void mul_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* out);

/**
 * Divide two big integers and save the result to out
 * @param left Left operand
 * @param right Right operand
 * @param out Output
 */
extern void div_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* out);

/**
 * Divide two big integers and save the quotient to "quotient" and the remainder to "remainder"
 * @param left Left operand
 * @param right Right operand
 * @param quotient Quotient output
 * @param remainder Remainder output
 */
extern void div_rem_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* quotient, MtMath_BigInt* remainder);

/**
 * Divide two big integers and save the remainder to out
 * @param left Left operand
 * @param right Right operand
 * @param out Output
 */
extern void rem_big_int(const MtMath_BigInt* left, const MtMath_BigInt* right, MtMath_BigInt* out);

/**
 * Convert a big integer to a long long
 * @param val Big int to convert
 */
extern long long big_int_ll(const MtMath_BigInt* val);

/**
 * Convert a big integer to a string.
 * Allocates memory, caller is responsible for deallocation.
 * @param val Big integer value to convert
 * @param out Where to allocate memory
 */
extern void big_int_str_alloc(const MtMath_BigInt* val, char** out);

/**
 * Convert a big integer to a string.
 * Saves into a buffer of bufferSize. If output is too large, will truncate.
 * Does not allocate memory.
 * @param val Big integer value to convert
 * @param buffer Buffer for output
 * @param bufferSize Size of buffer
 */
extern void big_int_str(const MtMath_BigInt* val, char* buffer, unsigned long long bufferSize);

/**
 * Convert a big integer to a string of a specific radix.
 * Allocates memory, caller is responsible for deallocation.
 * @param val Big integer value to convert
 * @param out Where to allocate memory
 * @return If conversion was successful
 */
extern bool big_int_str_alloc_rdx(const MtMath_BigInt* val, char** out, int radix);

/**
 * Convert a big integer to a string of a specific radix.
 * Saves into a buffer of bufferSize. If output is too large, will truncate.
 * Does not allocate memory.
 * @param val Big integer value to convert
 * @param buffer Buffer for output
 * @param bufferSize Size of buffer
 * @return If conversion was successful
 */
extern bool big_int_str_rdx(const MtMath_BigInt* val, char* buffer, unsigned long long bufferSize, int radix);

/**
 * Sets a rational number to have a specific numerator and denominator
 * @param ra Output rational number
 * @param numerator Numerator
 * @param denominator Denominator
 */
extern void set_rational(MtMath_Rational* ra, const MtMath_BigInt* numerator, const MtMath_BigInt* denominator);

/**
 * Adds two rational numbers and puts the result in out
 * @param left Left operand
 * @param right Right operand
 * @param out Resulting rational
 */
extern void add_rational(const MtMath_Rational* left, const MtMath_Rational* right, MtMath_Rational* out);

/**
 * Subtracts two rational numbers and puts the result in out
 * @param left Left operand
 * @param right Right operand
 * @param out Resulting rational
 */
extern void sub_rational(const MtMath_Rational* left, const MtMath_Rational* right, MtMath_Rational* out);

/**
 * Multiplies two rational numbers and puts the result in out
 * @param left Left operand
 * @param right Right operand
 * @param out Resulting rational
 */
extern void mul_rational(const MtMath_Rational* left, const MtMath_Rational* right, MtMath_Rational* out);

/**
 * Divides two rational numbers and puts the result in out
 * @param left Left operand
 * @param right Right operand
 * @param out Resulting rational
 */
extern void div_rational(const MtMath_Rational* left, const MtMath_Rational* right, MtMath_Rational* out);

#ifdef __cplusplus
};
#endif

#endif